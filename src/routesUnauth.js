import { lazy } from 'react';

const SignIn = lazy(() => import('./pages/auth/SignIn'));
const SignUp = lazy(() => import('./pages/auth/SignUp'));

export default [
  {
    path: '/',
    exact: true,
    component: SignIn
  },
  {
    path: '/sign-up',
    exact: true,
    component: SignUp
  }
];
