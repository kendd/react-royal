import React from 'react';
import HomeIcon from '@App/library/layout/icons/home';
import NewsIcon from '@App/library/layout/icons/news';
import ChecklistIcon from '@App/library/layout/icons/checklist';
import EnvelopIcon from '@App/library/layout/icons/envelop';
import TemplateIcon from '@App/library/layout/icons/template';
import TutorialIcon from '@App/library/layout/icons/tutorial';

export default function () {
  return [
    {
      title_const: 'home',
      to: '/',
      alternative_active_routes: [
        '/diagnostic',
        '/register-treatments',
        '/people-rights'
      ],
      icon: <HomeIcon />,
      exact: true
    },
    {
      title_const: 'general_informations',
      to: '/general-informations',
      icon: <NewsIcon />,
      exact: true
    },
    {
      title_const: 'action_plan',
      to: '/action-plan',
      icon: <ChecklistIcon />,
      exact: true
    },
    {
      title_const: 'watch_and_news',
      to: '/watch-and-news',
      icon: <EnvelopIcon />,
      exact: true
    },
    {
      title_const: 'documents_and_templates',
      to: '/tutorial',
      icon: <TemplateIcon />,
      exact: true
    },
    {
      title_const: 'tutorial',
      to: '/documents-and-templates',
      icon: <TutorialIcon />,
      exact: true
    }
  ];
}
