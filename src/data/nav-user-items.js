import React from 'react';
import IconUser from '@App/library/layout/icons/user';
import IconSetting from '@App/library/layout/icons/setting';
import IconQuestion from '@App/library/layout/icons/question';
import IconLogout from '@App/library/layout/icons/logout';

export default function () {
  return [
    {
      title: 'My Account',
      to: '/account',
      icon: <IconUser />
    },
    {
      title: 'Setting & Privacy',
      to: '/settings',
      icon: <IconSetting />
    },
    {
      title: 'Help & Support',
      to: '/help',
      icon: <IconQuestion />
    },
    {
      title: 'Sign Out',
      to: '/sign-out',
      icon: <IconLogout />
    }
  ];
}
