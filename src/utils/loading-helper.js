let loadingCount = 0;

const checkLoadingStatus = () => {
  if (loadingCount > 0) {
    document.body.classList.add('loading');
  } else {
    document.body.classList.remove('loading');

    // Alwasy set to 0 even if it is a negative
    loadingCount = 0;
  }
};

const addLoadingTop = (count = 1) => {
  loadingCount += count;
  checkLoadingStatus();
};

const removeLoadingTop = (count = 1) => {
  loadingCount -= count;
  setTimeout(() => {
    checkLoadingStatus();
  }, 500);
};

export { addLoadingTop, removeLoadingTop };
