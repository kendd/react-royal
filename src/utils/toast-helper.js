import { Slide, toast } from 'react-toastify';

const options = {
  position: 'bottom-right',
  autoClose: 8000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined
};

const errorToast = (message) => {
  return toast.error(message, options);
};

const successToast = (message) => {
  return toast.success(message, options);
};

const inforToast = (message) => {
  return toast.info(message, options);
};

const warningToast = (message) => {
  return toast.warn(message, options);
};

const defaultToast = (message) => {
  return toast(message, options);
};

const notificationToast = (message) => {
  return toast(message, {
    className: 'toast-notification',
    position: 'bottom-right',
    autoClose: 12000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    transition: Slide
  });
};

export {
  errorToast,
  successToast,
  inforToast,
  warningToast,
  defaultToast,
  notificationToast
};
