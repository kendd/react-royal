import { getData as getLocalStorageData } from './local-storage-helper';
import axios from 'axios';

const axiosInstance = axios.create();

// Request interceptor for API calls
axiosInstance.interceptors.request.use(
  async (config) => {
    const authInfo = getLocalStorageData('AUTH_INFO');
    let headerToken = {};

    if (authInfo) {
      headerToken = { Authorization: `Bearer ${authInfo}` };
    }

    config.headers = {
      ...headerToken,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Response interceptor for API calls
// axiosInstance.interceptors.response.use(
//   (response) => {
//     return response;
//   },
//   async (error) => {
//     const originalRequest = error.config;
//     if (error.response.status === 401 && !originalRequest._retry) {
//       const authInfo = getLocalStorageData('AUTH_INFO');
//       originalRequest._retry = true;
//       try {
//         const res = await refreshAccessToken();
//         const { accessToken, refreshToken } = res.data.data;
//         axios.defaults.headers.common.Authorization = 'Bearer ' + accessToken;
//         axios.defaults.headers.common.refresh_token = refreshToken;

//         // Save tokens to Localstorage
//         storeData(
//           {
//             ...authInfo,
//             accessToken,
//             refreshToken
//           },
//           'AUTH_INFO'
//         );

//         return axiosInstance(originalRequest);
//       } catch (e) {
//         removeAllData();
//         historyHelper.push('/sign-out');
//         window.location.reload();
//         return Promise.reject(e);
//       }
//     }
//     return Promise.reject(_get(error, 'response.data', ''));
//   }
// );

const fetch = axiosInstance;

export default fetch;
