import { getData } from './local-storage-helper';
import { get as _get } from 'lodash';

const getUserFullName = (userInfor) => {
  const user = userInfor || getData('AUTH_INFO');
  if (_get(user, 'name') && _get(user, 'first_name')) {
    return _get(user, 'name') + ' ' + _get(user, 'first_name');
  }
  return _get(user, 'email');
};

const getUserName = () => {
  const data = getData('AUTH_INFO');
  return _get(data.user, 'username');
};
export { getUserFullName, getUserName };
