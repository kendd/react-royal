const storeData = (data, key) => {
  if (!window.localStorage || !window.JSON || !key) {
    return;
  }

  window.localStorage.setItem(key, JSON.stringify(data));
};

const getData = (key) => {
  if (!window.localStorage || !window.JSON || !key) {
    return;
  }
  var item = window.localStorage.getItem(key);

  if (!item) {
    return;
  }

  return JSON.parse(item);
};

const removeData = (key) => {
  if (!window.localStorage || !window.JSON || !key) {
    return;
  }
  window.localStorage.removeItem(key);
};

const removeAllData = () => {
  if (!window.localStorage || !window.JSON) {
    return;
  }
  window.localStorage.clear();
};

export { storeData, getData, removeData, removeAllData };
