import fetch from './axios-helper';

function deleteData(url, optsHeader = {}) {
  return fetch.delete(url, {
    headers: {
      ...optsHeader
    }
  });
}

function putData(url, data = {}, optsHeader = {}) {
  return fetch.put(url, data, {
    headers: {
      ...optsHeader
    }
  });
}

function patchData(url, data = {}, optsHeader = {}) {
  return fetch.patch(url, data, {
    headers: {
      ...optsHeader
    }
  });
}

const postData = (url, data = {}, optsHeader = {}) => {
  return fetch.post(url, data, {
    headers: {
      ...optsHeader
    }
  });
};

function getData(url, optsHeader = {}) {
  return fetch.get(url, {
    headers: {
      ...optsHeader
    }
  });
}

export { getData, putData, patchData, postData, deleteData };
