import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { mapToCssModules, tagPropType } from './bootstrap/utils';
import Pagination from './bootstrap/Pagination';
import PaginationItem from './bootstrap/PaginationItem';
import PaginationLink from './bootstrap/PaginationLink';

const propTypes = {
  tag: tagPropType,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  totalItems: PropTypes.number,
  itemsPerPage: PropTypes.number,
  totalPages: PropTypes.array,
  currentPage: PropTypes.number,
  onChangePage: PropTypes.func,
  innerRef: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.func
  ])
};

const defaultProps = {
  tag: Pagination
};

const PaginationBar = (props) => {
  const {
    totalItems,
    className,
    cssModule,
    innerRef,
    onChangePage,
    itemsPerPage,
    currentPage,
    tag: Tag,
    ...attributes
  } = props;
  const totalPages = [];

  for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
    totalPages.push(i);
  }

  const classes = mapToCssModules(
    classNames(className, 'justify-content-center', 'd-flex'),
    cssModule
  );

  const onChange = (number, e) => {
    e.preventDefault();
    onChangePage(Number(number));
  };

  return (
    totalItems > itemsPerPage && (
      <Tag {...attributes} className={classes} ref={innerRef}>
        {totalPages.map((number, i) => (
          <PaginationItem key={i} active={currentPage === number}>
            <PaginationLink
              onClick={(e) => {
                onChange(number, e);
              }}
              href='#'
            >
              {number}
            </PaginationLink>
          </PaginationItem>
        ))}
      </Tag>
    )
  );
};

PaginationBar.propTypes = propTypes;
PaginationBar.defaultProps = defaultProps;

export default PaginationBar;
