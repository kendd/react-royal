import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { mapToCssModules, tagPropType } from './bootstrap/utils';
import { getData } from '@App/utils/local-storage-helper';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

const propTypes = {
  tag: tagPropType,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  footer: PropTypes.node,
  innerRef: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.func
  ])
};

const defaultProps = {
  tag: 'div'
};

const ContentWrapper = (props) => {
  const {
    className,
    cssModule,
    innerRef,
    tag: Tag,
    children,
    footer,
    ...attributes
  } = props;
  const classes = mapToCssModules(
    classNames(className, 'content-wrapper', footer && 'with-footer'),
    cssModule
  );
  const options = getData('OPTIONS');

  useEffect(() => {
    if (
      !_isEmpty(options, 'show_quick_access') &&
      document.getElementsByClassName('footer--quickaccess-spacing')[0]
    ) {
      const quickAccessFooterSpacingEl = document.getElementsByClassName(
        'footer--quickaccess-spacing'
      )[0];
      if (_get(options, 'show_quick_access', false)) {
        quickAccessFooterSpacingEl.classList.remove('hidden');
      } else {
        quickAccessFooterSpacingEl.classList.add('hidden');
      }
    }
  });

  return (
    <Tag {...attributes} className={classes} ref={innerRef}>
      <div className='content-wrapper__content'>{children}</div>
      {footer && (
        <div className='content-wrapper__footer'>
          <div className='footer--content'>{footer}</div>
        </div>
      )}
    </Tag>
  );
};

ContentWrapper.propTypes = propTypes;
ContentWrapper.defaultProps = defaultProps;

export default ContentWrapper;
