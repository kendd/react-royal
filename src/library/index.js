import React, { Component } from 'react';
import {
  CardBody,
  Card,
  CardTitle,
  Button,
  ContentWrapper,
  SummaryCard,
  FormGroup,
  Label,
  CustomInput
} from '@App/library/components';
import './index.scoped.scss';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';

const ExampleWrapperBox = (props) => {
  return (
    <Card className='example'>
      <CardBody>
        <CardTitle>{props.title}</CardTitle>
        {props.children}
      </CardBody>
    </Card>
  );
};

class UIComponents extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ContentWrapper>
        <ExampleWrapperBox title='Buttons'>{button()}</ExampleWrapperBox>
        <ExampleWrapperBox title='Summary Card'>
          {summaryCard()}
        </ExampleWrapperBox>
        <ExampleWrapperBox title='Forms'>{form()}</ExampleWrapperBox>
      </ContentWrapper>
    );
  }
}

const button = () => {
  return (
    <>
      <div>
        <Button color='primary'>Primary</Button>{' '}
        <Button color='primary' outline>
          Primary Outline
        </Button>
      </div>
      <SyntaxHighlighter language='javascript' className='mt-4' style={docco}>
        {`import React from 'react';
import { Button } from '@App/library/components';

const Example = (props) => {
  return (
    <div>
      <Button color='primary'>Primary</Button>
      <Button color='primary' outline>Primary Outline</Button>
    </div>
  );
}

export default Example;`}
      </SyntaxHighlighter>
    </>
  );
};

const summaryCard = () => {
  return (
    <>
      <div style={{ maxWidth: '200px', marginBottom: '30px' }}>
        <SummaryCard
          lg
          notification='99+'
          icon='calendar'
          title='TODAY APPOINTMENTS'
          amount={5}
        />
      </div>
      <div style={{ maxWidth: '140px' }}>
        <SummaryCard title='Last week Sales' amount={17} />
      </div>
      <SyntaxHighlighter language='javascript' className='mt-4' style={docco}>
        {`import React from 'react';
import { SummaryCard } from '@App/library/components';

const Example = (props) => {
  return (
    <div>
      <SummaryCard
        lg
        notification='99+'
        icon='calendar'
        title='TODAY APPOINTMENTS'
        amount='05'
      />
      <SummaryCard
        title='TODAY APPOINTMENTS'
        amount='05'
      />
    </div>
  );
}

export default Example;`}
      </SyntaxHighlighter>
    </>
  );
};

const form = () => {
  return (
    <>
      <FormGroup inline>
        <Label for='exampleCheckbox'>Radios</Label>
        <div>
          <CustomInput
            type='radio'
            id='exampleCustomRadio'
            name='customRadio'
            label='Select this custom radio'
          />
          <CustomInput
            type='radio'
            id='exampleCustomRadio2'
            name='customRadio'
            label='Or this one'
          />
          <CustomInput
            type='radio'
            id='exampleCustomRadio3'
            label='But not this disabled one'
            disabled
          />
          <CustomInput
            type='radio'
            id='exampleCustomRadio4'
            label="Can't click this label to select!"
            htmlFor='exampleCustomRadio4_X'
            disabled
          />
        </div>
      </FormGroup>
      <FormGroup>
        <Label for='exampleCheckbox'>Inline Radios</Label>
        <div>
          <CustomInput
            type='radio'
            id='exampleCustomRadio5'
            name='customRadio2'
            label='Select this custom radio'
            inline
          />
          <CustomInput
            type='radio'
            id='exampleCustomRadio6'
            name='customRadio2'
            label='Or this one'
            inline
          />
        </div>
      </FormGroup>
    </>
  );
};
export default UIComponents;
