import React from 'react';

function IconInfo() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='12'
      height='12'
      fill='none'
      viewBox='0 0 12 12'
    >
      <path
        fill='#BBBBBB'
        d='M6 0a6 6 0 100 12A6 6 0 006 0zm.6 8.4a.6.6 0 11-1.2 0v-3a.6.6 0 011.2 0v3zM6 4.2A.6.6 0 116 3a.6.6 0 010 1.2z'
      />
    </svg>
  );
}

export default IconInfo;
