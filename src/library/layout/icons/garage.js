import React from 'react';

function IconGarage() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='12'
      height='13'
      fill='none'
      viewBox='0 0 12 13'
    >
      <path
        fill='#BBB'
        d='M12 13h-1.714V5.2H1.714V13H0V3.467L6 0l6 3.467V13zM2.571 6.067H9.43V7.8H2.57V6.067zm0 2.6H9.43V10.4H2.57V8.667zm6.858 2.6V13H2.57v-1.733H9.43z'
      />
    </svg>
  );
}

export default IconGarage;
