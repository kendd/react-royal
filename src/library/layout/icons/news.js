import React from 'react';

function NewsIcon() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='28'
      height='28'
      fill='none'
      viewBox='0 0 28 28'
    >
      <g fill='#fff' clipPath='url(#clip0)'>
        <path d='M19.621 4.516H3.543a1.06 1.06 0 00-1.059 1.059v21.367A1.06 1.06 0 003.543 28H19.62a1.06 1.06 0 001.058-1.058V5.575a1.06 1.06 0 00-1.058-1.059zM16.033 19.42h-8.58a.903.903 0 110-1.806h8.58a.903.903 0 110 1.806zm0-3.613h-8.58a.903.903 0 110-1.806h8.58a.903.903 0 110 1.806zm0-3.612h-8.58a.903.903 0 110-1.807h8.58a.903.903 0 110 1.806z'></path>
        <path d='M25.516 2.71v19.87c0 1.365-1.097 2.71-3.03 2.71V5.575A2.868 2.868 0 0019.62 2.71H5.515C5.515 1.215 6.802 0 8.385 0h14.261c1.582 0 2.87 1.215 2.87 2.71z'></path>
      </g>
      <defs>
        <clipPath id='clip0'>
          <path fill='#fff' d='M0 0H28V28H0z'></path>
        </clipPath>
      </defs>
    </svg>
  );
}

export default NewsIcon;
