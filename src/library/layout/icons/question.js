import React from 'react';

function IconQuestion() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='22'
      height='22'
      fill='none'
      viewBox='0 0 22 22'
    >
      <path
        fill='#fff'
        fillRule='evenodd'
        d='M0 11C0 4.935 4.935 0 11 0s11 4.935 11 11-4.935 11-11 11S0 17.065 0 11zm12.1.92v1.28a1.099 1.099 0 11-2.2 0V11c0-.608.492-1.1 1.1-1.1.91 0 1.65-.74 1.65-1.65 0-.91-.74-1.65-1.65-1.65-.91 0-1.65.74-1.65 1.65a1.099 1.099 0 11-2.2 0A3.854 3.854 0 0111 4.4a3.854 3.854 0 013.85 3.85c0 1.738-1.166 3.193-2.75 3.67zM11 17.6a1.099 1.099 0 110-2.2 1.099 1.099 0 110 2.2z'
        clipRule='evenodd'
      />
    </svg>
  );
}

export default IconQuestion;
