import React from 'react';

function TutorialIcon() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='21'
      height='21'
      fill='none'
      viewBox='0 0 21 21'
    >
      <path
        fill='#fff'
        d='M11.375 15.75v1.75h3.5v1.75h-8.75V17.5h3.5v-1.75h-7a.875.875 0 01-.875-.875V3.5a.875.875 0 01.875-.875h15.75a.875.875 0 01.875.875v11.375a.875.875 0 01-.875.875h-7zM8.75 6.562v5.25l4.375-2.624L8.75 6.562z'
      />
    </svg>
  );
}

export default TutorialIcon;
