import React from 'react';

function IconUser() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='13'
      height='13'
      fill='none'
      viewBox='0 0 13 13'
    >
      <path
        fill='#BBB'
        d='M6.208 6.208a3.103 3.103 0 100-6.208 3.103 3.103 0 100 6.208zm0 1.552C4.136 7.76 0 8.8 0 10.864v1.552h12.416v-1.552c0-2.064-4.136-3.104-6.208-3.104z'
      />
    </svg>
  );
}

export default IconUser;
