import React from 'react';

function HomeIcon() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='24'
      height='23'
      fill='none'
      viewBox='0 0 24 23'
    >
      <path
        fill='#2887C7'
        d='M23.276 8.961l-8.951-7.955a3.5 3.5 0 00-4.65 0L.725 8.96A1.168 1.168 0 001.5 11h2.334v8.166a3.504 3.504 0 003.5 3.5H8.5v-7c0-4.624 7-4.626 7 0v7h1.167a3.504 3.504 0 003.5-3.5V11H22.5a1.168 1.168 0 00.776-2.039z'
      />
    </svg>
  );
}

export default HomeIcon;
