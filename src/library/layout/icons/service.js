import React from 'react';

function IconService() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='14'
      height='14'
      fill='none'
      viewBox='0 0 14 14'
    >
      <path
        fill='#BBB'
        d='M13.14 7.809l-.957-2.873A2.096 2.096 0 0010.19 3.5H3.81a2.097 2.097 0 00-1.992 1.436L.86 7.809A1.4 1.4 0 000 9.1v2.1c0 .517.283.963.7 1.206v.894a.7.7 0 00.7.7h.7a.7.7 0 00.7-.7v-.7h8.4v.7a.7.7 0 00.7.7h.7a.7.7 0 00.7-.7v-.894A1.393 1.393 0 0014 11.2V9.1a1.4 1.4 0 00-.86-1.291zM3.81 4.9h6.382a.7.7 0 01.664.479l.774 2.321H2.371l.773-2.321A.7.7 0 013.81 4.9zM2.45 11.2a1.05 1.05 0 110-2.1 1.05 1.05 0 010 2.1zm9.1 0a1.05 1.05 0 110-2.1 1.05 1.05 0 010 2.1zM2.294 2.1c.243.417.69.7 1.206.7.517 0 .963-.283 1.206-.7h4.589c.242.417.688.7 1.205.7.517 0 .963-.283 1.206-.7H10.5V.7h1.206c-.243-.416-.69-.7-1.206-.7-.517 0-.963.283-1.206.7H4.706C4.463.284 4.016 0 3.5 0c-.517 0-.963.283-1.206.7H3.5v1.4H2.294z'
      />
    </svg>
  );
}

export default IconService;
