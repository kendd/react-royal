import { getData } from '@App/utils/local-storage-helper';
import React from 'react';
import './index.scoped.scss';
import { AUTH_INFO } from '@App/config';

const Layout = (props) => {
  const authenticated = props.authenticated;
  const token = getData(AUTH_INFO);

  if (authenticated && token) {
    return <>{props.children}</>;
  }

  return <div className='auth-layout'>{props.children}</div>;
};

export default Layout;
