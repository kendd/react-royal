import React from 'react';

function IconArrowNext() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='8'
      height='14'
      fill='none'
      viewBox='0 0 8 14'
    >
      <path
        fill='#FF892B'
        d='M7.272 6.286a1 1 0 010 1.428l-5.984 5.87a.605.605 0 01-.848-.864l5.104-5.006a1 1 0 000-1.428L.44 1.28a.605.605 0 11.848-.864l5.984 5.87z'
      />
    </svg>
  );
}

export default IconArrowNext;
