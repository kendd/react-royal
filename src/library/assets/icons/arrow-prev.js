import React from 'react';

function IconArrowPrev() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='8'
      height='14'
      fill='none'
      viewBox='0 0 8 14'
    >
      <path
        fill='#FF892B'
        d='M.728 7.714a1 1 0 010-1.428L6.712.416a.605.605 0 11.848.864L2.456 6.286a1 1 0 000 1.428L7.56 12.72a.605.605 0 11-.848.864L.728 7.714z'
      />
    </svg>
  );
}

export default IconArrowPrev;
