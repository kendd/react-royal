/* eslint-disable prettier/prettier */
import { postData } from '@App/utils/fech-api';
import { API_URL } from '@App/config';

const login = (userLoginInfor) => {
  return postData(`${API_URL}/login`, userLoginInfor);
};

const authService = {
  login,
};

export default authService;
