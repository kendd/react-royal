/* eslint-disable prettier/prettier */
import { getData } from '@App/utils/fech-api';
import { API_URL } from '@App/config';

const getUsers = (userLoginInfor) => {
  return getData(`${API_URL}/users`, userLoginInfor);
};

const usersService = {
  getUsers
};

export default usersService;
