import { ACTIONS } from '../actions';

const defaultState = {};

export function usersReducer(state = defaultState, action) {
  switch (action.type) {
    case ACTIONS.GET_USERS:
      return {
        ...state,
        users: action.payload
      };
    default:
      return state;
  }
}
