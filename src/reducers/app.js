import { ACTIONS } from '../actions';

const defaultState = {
  appName: 'React Royal',
  token: null,
  viewChangeCounter: 0,
  language: 'fr',
  showQuickAccess: true
};

export function appsReducer(state = defaultState, action) {
  switch (action.type) {
    case ACTIONS.APP_LOAD:
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
        currentUser: action.payload ? action.payload.user : null
      };

    case ACTIONS.CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.payload === 0 ? 'en' : 'fr'
      };

    case ACTIONS.LOGIN:
      return {
        ...state,
        token: action.payload
      };
    case ACTIONS.REGISTER:
      return {
        ...state,
        token: action.payload
      };
    case ACTIONS.FORGOT_PASSWORD:
      return {
        ...state,
        token: action.payload
      };
    case ACTIONS.LOGOUT:
      return {
        ...state,
        token: null
      };
    case ACTIONS.UPDATE_TOKEN:
      return {
        ...state,
        token: { ...state.token, ...action.payload }
      };
    case ACTIONS.TOGGLE_QUICKACCESS:
      return {
        ...state,
        showQuickAccess: !state.showQuickAccess
      };
    default:
      return state;
  }
}
