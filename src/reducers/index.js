import { combineReducers } from 'redux';
import { appsReducer } from './app';
import { usersReducer } from './users';

export default combineReducers({
  appsReducer,
  usersReducer
});
