import React, { useEffect, Suspense, lazy } from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
  useLocation
} from 'react-router-dom';
import routes from './routes';
import routesUnauth from './routesUnauth';
import '@App/library/stylesheets/styles.scss';
import 'react-toastify/dist/ReactToastify.css';
import { get as _get, isEmpty as _isEmpty, filter as _filter } from 'lodash';
import { getData, removeAllData } from '@App/utils/local-storage-helper';
import { AUTH_INFO, VERSION } from '@App/config';
import Layout from '@App/library/layout';
import { version } from '../package.json';
import { ContentWrapper } from './library/components';
const NotFound = lazy(() => import('./pages/404'));

const DelayedFallback = () => {
  return <ContentWrapper />;
};

const mapStateToProps = (state) => ({
  app: state.appsReducer
});

const actionsToProps = {};

const layoutExceptionRoutes = [''];

const RenderApp = (props) => {
  const location = useLocation();
  const authenticated = props.authenticated;
  const appRoutes = authenticated ? routes : routesUnauth;

  const isInException = () => {
    const result = _filter(layoutExceptionRoutes, (route) => {
      return route === location.pathname;
    });
    return result.length > 0;
  };

  const checkVersion = () => {
    if (authenticated) {
      // Validate localstorage versioning
      const cacheVersion = getData(VERSION);
      const sourceVersion = version;
      if (cacheVersion !== sourceVersion) {
        removeAllData();
        window.location.reload();
      }
    }
  };

  useEffect(() => {
    checkVersion();
  }, [location]);

  return (
    <Layout authenticated={authenticated && !isInException()}>
      <Switch>
        {appRoutes.map((route, index) => {
          return (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={(props) => {
                return (
                  <Suspense fallback={<DelayedFallback />}>
                    <route.component {...props} />
                  </Suspense>
                );
              }}
            />
          );
        })}
        {authenticated && (
          <Suspense fallback={<DelayedFallback />}>
            <Route path='*' exact component={NotFound} />
          </Suspense>
        )}
        <Redirect to='/' />
      </Switch>
    </Layout>
  );
};

const App = (props) => {
  const token = getData(AUTH_INFO) || _get(props, 'app.token', '');

  return (
    <Router>
      <RenderApp authenticated={_isEmpty(token) === false} />
    </Router>
  );
};

export default connect(mapStateToProps, actionsToProps)(App);
