import { ACTIONS } from './index';
import authService from '@App/services/auth-service';
import { storeData, removeAllData } from '@App/utils/local-storage-helper';
import { AUTH_INFO, VERSION } from '@App/config';
import { errorToast } from '@App/utils/toast-helper';
import { version } from '../../package.json';

const loginAction = (userLoginInfor) => {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      authService
        .login(userLoginInfor)
        .then((result) => {
          if (result.data.token) {
            storeData(result.data.token, AUTH_INFO);
            storeData(version, VERSION);
            setTimeout(() => {
              dispatch({
                type: ACTIONS.LOGIN,
                payload: result.data.token
              });
            }, 0);
          }
          resolve();
        })
        .catch((err) => {
          errorToast(err.message);
          reject(err);
        });
    });
  };
};

const logOutAction = () => {
  return (dispatch) => {
    removeAllData();
    dispatch({
      type: ACTIONS.LOGOUT,
      payload: null
    });
  };
};

export { loginAction, logOutAction };
