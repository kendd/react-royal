import { ACTIONS } from './index';
import { errorToast } from '@App/utils/toast-helper';
import usersService from '@App/services/user-service';

const getUserAction = () => {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      usersService
        .getUsers()
        .then((result) => {
          dispatch({
            type: ACTIONS.GET_USERS,
            payload: result.data
          });
          resolve();
        })
        .catch((err) => {
          errorToast(err.message);
          reject(err);
        });
    });
  };
};

export { getUserAction };
