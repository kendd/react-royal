import React from 'react';
import './SignUp.scoped.scss';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import {
  Button,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label
} from '@App/library/components';
import { Link } from 'react-router-dom';

const SignUp = ({ onLogin, t }) => {
  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      password: ''
    },
    validationSchema: Yup.object({
      username: Yup.string().required('Required'),
      email: Yup.string().required('Required'),
      password: Yup.string().required('Required')
    }),
    onSubmit: (values) => {}
  });

  return (
    <div className='container-scroller'>
      <div className='container-fluid page-body-wrapper full-page-wrapper'>
        <div className='content-wrapper d-flex align-items-center auth px-0'>
          <div className='row w-100 mx-0'>
            <div className='col-lg-4 mx-auto'>
              <div className='auth-form-light text-left py-5 px-4 px-sm-5'>
                <div className='brand-logo'>
                  <img
                    src='http://www.templatewatch.com/royalui/template/images/logo.svg'
                    alt='logo'
                  />
                </div>
                <h4>New here?</h4>
                <h6 className='font-weight-light'>
                  Signing up is easy. It only takes a few steps
                </h6>
                <Form onSubmit={formik.handleSubmit} className='pt-3'>
                  <FormGroup>
                    <Input
                      type='text'
                      name='username'
                      placeholder='Username'
                      onChange={formik.handleChange}
                      value={formik.values.username}
                      onBlur={formik.handleBlur}
                      invalid={
                        formik.touched.username && !!formik.errors.username
                      }
                    />
                    <FormFeedback>{formik.errors.username}</FormFeedback>
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type='email'
                      name='email'
                      placeholder='Email'
                      onChange={formik.handleChange}
                      value={formik.values.email}
                      onBlur={formik.handleBlur}
                      invalid={formik.touched.email && !!formik.errors.email}
                    />
                    <FormFeedback>{formik.errors.email}</FormFeedback>
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type='password'
                      name='password'
                      placeholder='Password'
                      onChange={formik.handleChange}
                      value={formik.values.password}
                      onBlur={formik.handleBlur}
                      invalid={
                        formik.touched.password && !!formik.errors.password
                      }
                    />{' '}
                    <FormFeedback>{formik.errors.password}</FormFeedback>
                  </FormGroup>
                  <div className='mt-3'>
                    <Button
                      block
                      size='lg'
                      color='primary'
                      className='font-weight-medium auth-form-btn'
                      type='submit'
                    >
                      SIGN UP
                    </Button>
                  </div>
                  <div className='my-2 d-flex justify-content-between align-items-center'>
                    <FormGroup check>
                      <Label check className='text-muted'>
                        <Input type='checkbox' />
                        I agree to all Terms & Conditions
                        <i className='input-helper' />
                      </Label>
                    </FormGroup>
                  </div>
                  <div className='text-center mt-4 font-weight-light'>
                    Already have an account?{' '}
                    <Link to='/sign-in' className='text-primary'>
                      Login
                    </Link>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
