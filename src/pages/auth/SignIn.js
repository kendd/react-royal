import React from 'react';
import './SignIn.scoped.scss';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { loginAction } from '@App/actions/auth';
import { useFormik } from 'formik';
import {
  Button,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label
} from '@App/library/components';
import { Link } from 'react-router-dom';

const mapStateToProps = (state) => ({});

const mapActionToProps = {
  onLogin: loginAction
};

const SignIn = ({ onLogin }) => {
  const formik = useFormik({
    initialValues: {
      username: '',
      password: ''
    },
    validationSchema: Yup.object({
      username: Yup.string().required('Required'),
      password: Yup.string().required('Required')
    }),
    onSubmit: (values) => {
      onLogin(values);
    }
  });

  return (
    <div className='container-scroller'>
      <div className='container-fluid page-body-wrapper full-page-wrapper'>
        <div className='content-wrapper d-flex align-items-center auth px-0'>
          <div className='row w-100 mx-0'>
            <div className='col-lg-4 mx-auto'>
              <div className='auth-form-light text-left py-5 px-4 px-sm-5'>
                <div className='brand-logo'>
                  <img
                    src='http://www.templatewatch.com/royalui/template/images/logo.svg'
                    alt='logo'
                  />
                </div>
                <h4>Hello! let's get started</h4>
                <h6 className='font-weight-light'>Sign in to continue.</h6>
                <Form onSubmit={formik.handleSubmit} className='pt-3'>
                  <FormGroup>
                    <Input
                      type='text'
                      name='username'
                      placeholder='Username'
                      onChange={formik.handleChange}
                      value={formik.values.username}
                      onBlur={formik.handleBlur}
                      invalid={
                        formik.touched.username && !!formik.errors.username
                      }
                    />
                    <FormFeedback>{formik.errors.username}</FormFeedback>
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type='password'
                      name='password'
                      placeholder='Password'
                      onChange={formik.handleChange}
                      value={formik.values.password}
                      onBlur={formik.handleBlur}
                      invalid={
                        formik.touched.password && !!formik.errors.password
                      }
                    />{' '}
                    <FormFeedback>{formik.errors.password}</FormFeedback>
                  </FormGroup>
                  <div className='mt-3'>
                    <Button
                      block
                      size='lg'
                      color='primary'
                      className='font-weight-medium auth-form-btn'
                      type='submit'
                    >
                      SIGN IN
                    </Button>
                  </div>
                  <div className='my-2 d-flex justify-content-between align-items-center'>
                    <FormGroup check>
                      <Label check className='text-muted'>
                        <Input type='checkbox' /> Keep me signed in
                        <i className='input-helper' />
                      </Label>
                    </FormGroup>
                    <a href='#' className='auth-link text-black'>
                      Forgot password?
                    </a>
                  </div>
                  <div className='mb-2'>
                    <Button
                      type='button'
                      block
                      className='btn-facebook auth-form-btn'
                    >
                      <i className='ti-facebook mr-2' />
                      Connect using facebook
                    </Button>
                  </div>
                  <div className='text-center mt-4 font-weight-light'>
                    Don't have an account?{' '}
                    <Link to='/sign-up' className='text-primary'>
                      Create
                    </Link>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps, mapActionToProps)(SignIn);
