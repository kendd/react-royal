import React from 'react';
// import * as animationData from './animate.json';
// import Lottie from 'react-lottie';
import './index.scoped.scss';
import { withNamespaces } from 'react-i18next';

const NotFound = (props) => {
  // const defaultOptions = {
  //   loop: true,
  //   autoplay: true,
  //   animationData: animationData.default,
  //   rendererSettings: {
  //     preserveAspectRatio: 'xMidYMid slice'
  //   }
  // };

  return (
    <div className='wrapper'>
      <div className='title'>404</div>
      {/* <Lottie
        isClickToPauseDisabled
        options={defaultOptions}
        height={340}
        width={500}
        // isStopped={false}
        // isPaused={false}
      /> */}
      <div className='description'>{props.t('page_not_found')}!</div>
    </div>
  );
};

export default withNamespaces()(NotFound);
