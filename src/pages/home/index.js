import React, { useEffect, useState } from 'react';
import {
  Button,
  Card,
  CardBody,
  ContentWrapper,
  PaginationBar,
  Table
} from '@App/library/components';
import './index.scoped.scss';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { getUserAction } from '@App/actions/user';
import { logOutAction } from '@App/actions/auth';

const mapStateToProps = (state) => ({
  users: state.usersReducer.users
});

const mapActionToProps = {
  getUsers: getUserAction,
  logout: logOutAction
};

const Home = ({ users, getUsers, logout }) => {
  const [listUsers, setListUsers] = useState([]);
  const [total, setTotal] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 4;

  useEffect(() => {
    getUsers();
  }, []);

  useEffect(() => {
    if (users) {
      setTotal(users.length);
      setListUsers(users);
      setListUsers(paginate(users, itemsPerPage, 1));
    }
  }, [users]);

  const paginate = (list, pageSize, pageNumber) => {
    setCurrentPage(pageNumber);
    return list.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
  };

  return (
    <>
      <ContentWrapper>
        <Card>
          <CardBody>
            <Button color='danger' onClick={logout}>
              Logout
            </Button>
            <Table responsive striped className='mb-3'>
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Phone</th>
                </tr>
              </thead>
              <tbody>
                {listUsers.map((user, idx) => (
                  <tr key={idx}>
                    <td>{user.email}</td>
                    <td>{user.name}</td>
                    <td>{user.phone}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <PaginationBar
              totalItems={total}
              itemsPerPage={itemsPerPage}
              currentPage={currentPage}
              onChangePage={(e) =>
                setListUsers(paginate(users, itemsPerPage, e))
              }
            />
          </CardBody>
        </Card>
      </ContentWrapper>
    </>
  );
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withNamespaces()(Home));
