# SMC Protect

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To run in hot module reloading mode:

```sh
npm start
```

To create a production build:

```sh
npm run build:prod
```

To create a development build:

```sh
npm run build:dev
```

## Running

Open the file `dist/index.html` in your browser

## Design
https://www.figma.com/file/ObNLiaLknUcW9Yn8g2BFan/SMC-Project?node-id=267%3A352

### Version
# 0.0.1
- Initial project
